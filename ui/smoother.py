import sys
import PIL.Image as Image
from PIL import ImageFilter

src = sys.argv[1]

img = Image.open(src)
res = img.filter(ImageFilter.SMOOTH)
res.save(src, 'PNG')

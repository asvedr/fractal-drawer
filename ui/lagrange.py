import sys
import consts
from point import Point
if sys.version[0] == '2':
    is_py3 = False
    import Tkinter as tkinter
else:
    is_py3 = True
    import tkinter


class Lagrange(tkinter.Frame):

    '''
        Viewer for lafrange fun and lagrange calculator
        Use get_prepared_pts to get points from Widget
    '''

    @staticmethod
    def L(pts, x):
        ''' calculate Y for polynom X '''
        def mult(i):
            result = 1
            for j in range(len(pts)):
                if j == i:
                    continue
                result *= (x - pts[j].x) / (pts[i].x - pts[j].x)
            return result
        return sum([pts[i].y*mult(i) for i in range(len(pts))])

    def __init__(self, root, width, height):

        tkinter.Frame.__init__(self, root)

        canvas = tkinter.Canvas(self, width=width, height=height)
        canvas.pack()

        canvas.bind('<ButtonPress-1>', self.press_left)
        canvas.bind('<ButtonRelease-1>', self.release_left)
        canvas.bind('<B1-Motion>', self.mouse_move)
        canvas.bind('<ButtonPress-2>', self.click_right)
        canvas.bind('<ButtonPress-3>', self.click_right)

        self.canvas = canvas
        self.left_down = False
        self.active_point = None
        self.active_type = None
        self.points = [Point(0, consts.HEIGHT), Point(consts.WIDTH, 0)]

    def get_prepared_pts(self):
        ''' for saving. Points in [(0,0) .. (1,1)] '''
        def prepare(p):
            return Point(float(p.x) / consts.WIDTH,
                         float(consts.HEIGHT - p.y) / consts.HEIGHT)
        return [prepare(p) for p in self.points]

    def redraw(self):
        self.canvas.delete('all')
        self.canvas.create_rectangle(1, 1,
                                     consts.WIDTH - 1,
                                     consts.HEIGHT - 1,
                                     fill='white')
        if len(self.points) < 2:
            return
        result = []

        def prepare(p):
            return Point(float(p.x) / consts.WIDTH,
                         float(p.y) / consts.HEIGHT)
        prepared_pts = [prepare(p) for p in self.points]
        for x in range(consts.WIDTH):
            x1 = float(x) / consts.WIDTH
            y1 = Lagrange.L(prepared_pts, x1)
            y = int(y1 * consts.HEIGHT)
            result.append(Point(x, y))
        for i in range(len(result) - 1):
            a = result[i]
            b = result[i+1]
            self.canvas.create_line(a.x, a.y, b.x, b.y)
        for p in self.points:
            self.canvas.create_oval(p.x - 3, p.y - 3, p.x + 3, p.y + 3)

    def press_left(self, ev):
        ''' left mouse button '''
        pt = Point(ev.x, ev.y)
        self.left_down = True

        sq_dist = consts.DIST_TO_TOUCH ** 2
        for i in range(len(self.points)):
            if self.points[i].sq_dist(pt) < sq_dist:
                if i == 0:
                    self.active_type = 'left'
                elif i == len(self.points) - 1:
                    self.active_type = 'right'
                else:
                    self.active_type = None
                del self.points[i]
                break
        else:
            self.active_type = None

        self.active_point = pt

    def release_left(self, ev):
        ''' left mouse button '''
        self.left_down = False
        pt = self.active_point
        if self.active_type == 'left':
            pt.x = 0
        elif self.active_type == 'right':
            pt.x = consts.WIDTH
        self.active_point = None
        self.active_type = None
        bad_pt = None
        for i in range(len(self.points)):
            if abs(self.points[i].x - pt.x) < consts.MINIMAL_DIST:
                if i == 0:
                    pt.x = 0
                elif i == len(self.points) - 1:
                    pt.x = consts.WIDTH
                bad_pt = i
                break
        if bad_pt is not None:
            del self.points[bad_pt]
        self.points.append(pt)
        if is_py3:
            self.points.sort(key=lambda a: a.x)
        else:
            self.points.sort(cmp=lambda a, b: cmp(a.x, b.x))
        # assert False, 'ADD NEAR X CHECK HERE'
        self.redraw()

    def click_right(self, ev):
        ''' right mouse button '''
        if len(self.points) == 0:
            return
        pt = Point(ev.x, ev.y)
        del_i = 0
        dist = consts.DIST_TO_TOUCH ** 2
        for i in range(len(self.points)):
            d = pt.sq_dist(self.points[i])
            if d < dist:
                del_i = i
                break
        if i != 0 and i != len(self.points) - 1:
            del self.points[del_i]
        self.redraw()

    def mouse_move(self, ev):
        self.active_point.x = ev.x
        self.active_point.y = ev.y

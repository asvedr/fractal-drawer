# Not for install
# This file is just for build with cx_Freeze
# Call `python setup.py build`
from cx_Freeze import setup, Executable

base = None

executables = [Executable("main.py", base=base)]

packages = ["idna"]
options = {
    'build_exe': {
        'packages': packages
    },
}

setup(
    name="fractal_drawer",
    options=options,
    version="1.0",
    description='',
    executables=executables
)

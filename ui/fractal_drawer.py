import sys
import PIL
import PIL.Image
import PIL.ImageTk

import consts
from config import Config
import multiprocessing
import subprocess as SP
import os
if sys.version[0] == '2':
    is_py3 = False
    import Tkinter as tkinter
else:
    is_py3 = True
    import tkinter
    from functools import reduce


class Drawer(tkinter.Frame):

    def __init__(self, root):

        tkinter.Frame.__init__(self, root)
        self.canvas = tkinter.Canvas(self,
                                     width=consts.PIC_WIDTH,
                                     height=consts.PIC_HEIGHT)
        self.canvas.pack()
        self._GC = None

    def rm_tex(self):
        self._GC = None

    def update(self):
        ''' delete old pic, render new pic '''
        self.canvas.delete('all')
        self.canvas.create_rectangle(0, 0,
                                     consts.PIC_WIDTH,
                                     consts.PIC_HEIGHT,
                                     fill='white')
        tex = PIL.Image.open(Config.small_pic_file)
        pic = PIL.ImageTk.PhotoImage(tex)
        # Save objects to Drawer because of GC
        self._GC = (tex, pic)
        self.canvas.create_image(0, 0, image=pic, anchor=tkinter.NW)

    def _make_pic(self, state, out):
        '''
            state - struct with
                .size
                .depth
                .fun
                .x0
                .y0
                .mid_point
                .crd_window
                .lagrange
                .colors
            out - filename
        '''
        if Config.rust_render == '':
            raise Exception('Config.rust_render is None')

        thread_count = multiprocessing.cpu_count()

        # args: size depth fun xfrom,yfrom,xto,yto conffile thread_count file
        coords = '%s,%s,%s,%s' % (state.mid_point.x - state.crd_window.x,
                                  state.mid_point.y - state.crd_window.y,
                                  state.mid_point.x + state.crd_window.x,
                                  state.mid_point.y + state.crd_window.y)

        xline = ''
        yline = ''
        for pt in state.lagrange:
            xline += '%s ' % pt.x
            yline += '%s ' % pt.y

        with open(Config.param_file, 'wt') as header:
            header.write('x:%s\n' % xline.strip())
            header.write('y:%s\n' % yline.strip())
            header.write('depth:%s\n' % state.depth)
            header.write('size:%s\n' % state.size)
            header.write('fun:%s\n' % state.fun)
            header.write('colors:%s\n' % reduce('{} {}'.format, state.colors))
            header.write('coords:%s\n' % coords)
            header.write('thread_count:%s\n' % thread_count)
            header.write('out:%s\n' % out)
            header.write('x0:%s\n' % state.x0)
            header.write('y0:%s\n' % state.y0)

        assert os.path.isfile(Config.rust_render)
        call = [Config.rust_render, Config.param_file]
        ans = SP.check_output(list(map(str, call)))
        self._parse_ans(ans)

    def gen_new_pic(self, state):
        ''' render small pic '''
        self._make_pic(state, Config.small_pic_file)

    def save(self, state):
        ''' render big pic '''
        self._make_pic(state, Config.big_pic_file)

    def _parse_ans(self, ans):
        if ans.decode()[0:2] != 'Ok':
            raise Exception('RENDER ERROR', ans.decode())

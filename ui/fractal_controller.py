import sys
if sys.version[0] == '2':
    import Tkinter as tkinter
else:
    import tkinter


class Controller(tkinter.Frame):
    ''' Contol buttons for moving in fractal and zoom
        To use set callback .onmove and .onzoom outsize
    '''

    def __init__(self, root):

        tkinter.Frame.__init__(self, root)

        left_frame = tkinter.Frame(self)
        left_frame.pack(side=tkinter.LEFT)
        center_frame = tkinter.Frame(self)
        center_frame.pack(side=tkinter.LEFT)
        right_frame = tkinter.Frame(self)
        right_frame.pack(side=tkinter.LEFT)

        top_frame = tkinter.Frame(center_frame)
        top_frame.pack(side=tkinter.TOP)

        zoom_frame = tkinter.Frame(center_frame)
        zoom_frame.pack(side=tkinter.TOP)

        bottom_frame = tkinter.Frame(center_frame)
        bottom_frame.pack(side=tkinter.TOP)

        tkinter.Button(left_frame,
                       text='<',
                       height=6,
                       command=lambda: self.onmove(-1, 0)).pack()
        tkinter.Button(right_frame,
                       text='>',
                       height=6,
                       command=lambda: self.onmove(1, 0)).pack()
        tkinter.Button(top_frame,
                       text='^',
                       width=6,
                       command=lambda: self.onmove(0, -1)).pack()
        tkinter.Button(bottom_frame,
                       text='v',
                       width=6,
                       command=lambda: self.onmove(0, 1)).pack()

        tkinter.Button(zoom_frame,
                       command=lambda: self.onzoom(0.5),
                       text='+').pack(side=tkinter.LEFT)
        tkinter.Button(zoom_frame,
                       command=lambda: self.onzoom(2),
                       text='-').pack(side=tkinter.LEFT)

        def default_callback(*_):
            pass

        self.onmove = default_callback
        self.onzoom = default_callback

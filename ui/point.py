class Point(object):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

    def __mul__(self, K):
        return Point(self.x * K, self.y * K)

    def __repr__(self):
        return '<%s, %s>' % (self.x, self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __lt__(self, other):
        return self.x < other.x

    def sq_dist(self, other):
        return (self.x - other.x) ** 2 + (self.y - other.y) ** 2

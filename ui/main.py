import sys
import os
import re
from lagrange import Lagrange
import consts
from point import Point
from fractal_drawer import Drawer
import config
import fractal_controller
if sys.version[0] == '2':
    is_py3 = False
    import Tkinter as tkinter
else:
    is_py3 = True
    import tkinter
    from functools import reduce


def bezier(colors, x):
    folded = []
    source = colors
    assert type(x) == float
    while len(source) > 1:
        for i in range(len(source) - 1):
            A = source[i]
            B = source[i + 1]
            point = (A[i] + ((B[i] - A[i]) * x) for i in range(3))
            folded.append(tuple(point))
        source = folded
        folded = []
    return tuple(map(int, source[0]))


class State(object):
    size = 750
    depth = 100
    fun = 'man'
    x0 = 0
    y0 = 0
    mid_point = Point(0, 0)
    crd_window = Point(2, 2)
    lagrange = []
    colors = []


class UI(object):

    def __init__(self):
        # Template for validating
        self.color_template = re.compile(r'[0-9a-fA-F]' * 6)
        self.root = tkinter.Tk()
        # Left frame for all controllers
        scheme_frame = tkinter.Frame(self.root)
        scheme_frame.pack(side=tkinter.LEFT)
        # Right frame for render
        pic_frame = tkinter.Frame(self.root)
        pic_frame.pack(side=tkinter.RIGHT)
        # Frame for lagrange and depth
        view_frame = tkinter.Frame(scheme_frame)
        view_frame.pack(side=tkinter.TOP)
        # Frame for x0,y0 and fracral type
        params_frame = tkinter.Frame(view_frame)
        # Frame for buttons 'save' and 'render'
        button_frame = tkinter.Frame(scheme_frame)
        button_frame.pack(side=tkinter.TOP)
        # Button controller for moving and zoom
        controller = fractal_controller.Controller(scheme_frame)
        controller.pack(side=tkinter.TOP)
        # Setting callbacks
        controller.onmove = self.onmove
        controller.onzoom = self.onzoom
        # Contoller for color intensity
        self.lagrange = Lagrange(view_frame,
                                 width=consts.WIDTH,
                                 height=consts.HEIGHT)
        # Color list
        clrs_box = tkinter.Text(view_frame, width=70, height=6)
        clrs_box.insert(tkinter.END, '000000\nffffff')
        # Var for depth value
        depthtext = tkinter.StringVar()
        depth = tkinter.Entry(view_frame, textvariable=depthtext)
        depthtext.set('100')
        self.depth = depthtext
        save_btn = tkinter.Button(button_frame,
                                  text='save',
                                  command=self.save)
        render_btn = tkinter.Button(button_frame,
                                    text='render',
                                    command=self.redraw)
        save_btn.pack(side=tkinter.LEFT)
        render_btn.pack(side=tkinter.RIGHT)
        self.colors = clrs_box

        # Real fractal drawer
        self.drawer = Drawer(pic_frame)
        self.drawer.pack()

        self.x0var = tkinter.StringVar()
        self.x0var.set('0')
        self.y0var = tkinter.StringVar()
        self.y0var.set('0')
        tkinter.Entry(params_frame,
                      textvariable=self.x0var).pack(side=tkinter.LEFT)
        tkinter.Entry(params_frame,
                      textvariable=self.y0var).pack(side=tkinter.LEFT)

        self.funvar = tkinter.StringVar()
        self.funvar.set('man')
        tkinter.Radiobutton(params_frame,
                            text="man",
                            variable=self.funvar,
                            value="man").pack(side=tkinter.LEFT)
        tkinter.Radiobutton(params_frame,
                            text="jul",
                            variable=self.funvar,
                            value="jul").pack(side=tkinter.LEFT)

        # All drawer params
        state = State()
        self.state = state

        self.lagrange.pack(side=tkinter.TOP)
        clrs_box.pack(side=tkinter.TOP)
        depth.pack(side=tkinter.TOP)
        params_frame.pack(side=tkinter.TOP)

        self.lagrange.redraw()
        self.redraw()

    def get_colors(self):
        ''' getting color list from Text widget and validate it '''
        text = self.colors.get("1.0", tkinter.END)
        text = reduce(list.__add__,
                      map(lambda a: a.split(' '), text.split('\n')))
        text = list(filter(lambda t: len(t) > 0, text))
        for clr in text:
            if not self.color_template.match(clr):
                raise Exception('bad color %s' % repr(clr))
        return text

    def onmove(self, dx, dy):
        ''' move button click '''
        delta = Point(dx, dy) * (self.state.crd_window.x / 4.0)
        self.state.mid_point = self.state.mid_point + delta
        self.redraw()

    def onzoom(self, dz):
        ''' zoom button click '''
        self.state.crd_window = self.state.crd_window * dz
        self.redraw()

    def redraw(self):
        '''
            draw button click or other button
            uodate info in self.state and redraw for fractal drawer
        '''
        state = self.state
        state.lagrange = self.lagrange.get_prepared_pts()
        state.colors = self.get_colors()
        state.depth = int(self.depth.get())
        state.fun = self.funvar.get()
        state.x0 = float(self.x0var.get())
        state.y0 = float(self.y0var.get())
        self.drawer.gen_new_pic(state)
        self.drawer.update()

    def save(self):
        '''
            save button click
            save big picture to file
        '''
        self.redraw()
        state = self.state
        size = state.size
        state.size = 9000
        self.drawer.save(state)
        state.size = size

    def run(self):
        self.root.mainloop()


path = os.path.abspath(os.path.dirname(sys.argv[0]))
config.Config.rust_render = os.path.join(path, 'picgen.exe')
UI().run()

/// Functions for generating fractal data
pub type CrdFloat = f64;
/// Fn(x0, y0, x, y, max_depth_to_search) -> depth_on_failed
pub type RenderFun = Fn(CrdFloat, CrdFloat, CrdFloat, CrdFloat, usize) -> usize;

/// This is RenderFun
pub fn julia(x0 : CrdFloat, y0 : CrdFloat, x : CrdFloat, y : CrdFloat, depth : usize) -> usize {
    let mut zx = x;
    let mut zy = y;
    for i in 0 .. depth {
        let zx1 = (zx * zx) - (zy * zy) + x0;
        let zy1 = zx * zy * 2.0 + y0;
        if zx1 * zx1 + zy1 * zy1 > 4.0 {
            return i;
        }
        else {
            zx = zx1;
            zy = zy1;
        }
    }
    return depth;
}

/// This is RenderFun
pub fn mandelbort(x0 : CrdFloat, y0 : CrdFloat, x : CrdFloat, y : CrdFloat, depth : usize) -> usize {
    let mut zx = x0;
    let mut zy = y0;
    for i in 0 .. depth {
        let zx1 = (zx * zx) - (zy * zy) + x;
        let zy1 = zx * zy * 2.0 + y;
        if zx1 * zx1 + zy1 * zy1 > 4.0 {
            return i;
        }
        else {
            zx = zx1;
            zy = zy1;
        }
    }
    return depth;
}

extern crate image;

mod complex_funs;
mod polynoms;

use image::*;
use std::env;
use std::str::FromStr;
use std::fs::*;
use std::io;
use std::u32;
use complex_funs::*;
use polynoms::*;
use std::io::Read;
use std::thread;
use std::collections::HashMap;

/// Macro for parsing args
macro_rules! get_arg {
    ($val:expr, $mess:expr) =>
        (match $val {
            Ok(val) => val,
            _ => panic!("BAD ARG: {}", $mess),
        })
}

/// Input file params
struct Params {
    /// picture will be `size` x `size`
    size            : u32,
    /// max depth to search in fractal fun
    depth           : usize,
    /// window param
    x_from          : CrdFloat,
    /// window params
    y_from          : CrdFloat,
    /// window params
    x_dist          : CrdFloat,
    /// window params
    y_dist          : CrdFloat,
    /// how many thead use to calculate lines of picture(parallelism)
    thread_count    : u32,
    /// where to write result
    outfile         : String,
    /// colors for bezier/polynom calculating
    colors          : Vec<[u8; 3]>,
    /// ...
    intense_polynom : Vec<Point>,
    /// 'man' or 'jul'
    fun             : String,
    /// start params for function
    x0              : CrdFloat,
    /// start params for function
    y0              : CrdFloat
}

impl Params {
    fn load_file(path : &str) -> io::Result<Params> {
        let mut file = File::open(path)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        let lines : Vec<&str> = contents.split('\n').collect();
        let mut map = HashMap::new();
        map.reserve(lines.len());
        for line in lines {
            let split : Vec<&str> = line.split('\r').collect();
            let line = split[0];
            if line.len() == 0 {
                continue
            }
            let pair : Vec<&str> = line.split(':').collect();
            if pair.len() != 2 {
                panic!("bad config line: {}", line);
            }
            map.insert(pair[0].to_string(), pair[1].to_string());
        }

        macro_rules! get_line {
            ($key:expr) => {match map.get($key){
                None => panic!("config has no line {}", $key),
                Some(val) => val
            }}
        }

        let xs : Vec<&str> = get_line!("x").split(' ').collect();
        let ys : Vec<&str> = get_line!("y").split(' ').collect();
        let cs : Vec<&str> = get_line!("colors").split(' ').collect();
        let mut colors : Vec<[u8; 3]> = vec![];
        let mut intense_polynom = vec![];
        for i in 0 .. xs.len() {
            let x = f64::from_str(xs[i]).expect("point x");
            let y = f64::from_str(ys[i]).expect("point y");
            intense_polynom.push(Point{x: x, y: y});
        }
        for c in cs {
            let clr = u32::from_str_radix(c, 16).expect("color");
            let r   = (clr & 0xff0000) >> 16;
            let g   = (clr & 0x00ff00) >> 8;
            let b   =  clr & 0x0000ff;
            colors.push([r as u8, g as u8, b as u8]);
        }
        let window : Vec<&str> = get_line!("coords").split(',').collect();
        let x_from = get_arg!(CrdFloat::from_str(&window[0]), "xfrom");
        let y_from = get_arg!(CrdFloat::from_str(&window[1]), "xfrom");
        let x_to   = get_arg!(CrdFloat::from_str(&window[2]), "xfrom");
        let y_to   = get_arg!(CrdFloat::from_str(&window[3]), "xfrom");
        let x_dist = x_to - x_from;
        let y_dist = y_to - y_from;
        Ok(Params{
            colors          : colors,
            intense_polynom : intense_polynom,
            depth           : get_arg!(usize::from_str(get_line!("depth")), "depth"),
            size            : get_arg!(u32::from_str(get_line!("size")), "size"),
            thread_count    : get_arg!(u32::from_str(get_line!("thread_count")), "thread_count"),
            outfile         : get_line!("out").clone(),
            fun             : get_line!("fun").clone(),
            x_from          : x_from,
            y_from          : y_from,
            x_dist          : x_dist,
            y_dist          : y_dist,
            x0              : get_arg!(CrdFloat::from_str(get_line!("x0")), "x0"),
            y0              : get_arg!(CrdFloat::from_str(get_line!("y0")), "y0")
        })
    }
}

/// Parameters for every thread
struct RenderInput {
    /// window
    x_from  : CrdFloat,
    /// window
    x_dist  : CrdFloat,
    /// window
    y_from  : CrdFloat,
    /// window
    y_dist  : CrdFloat,
    /// start params for fun
    x0      : CrdFloat,
    /// start params for fun
    y0      : CrdFloat,
    /// pic size
    width   : u32,
    /// pic size
    height  : u32,
    /// colors for polynom
    colors  : Vec<[u8; 3]>,
    /// polynom points
    polynom : Vec<Point>,
    /// max depth to search
    depth   : usize,
    /// 'man' or 'jul'
    fun     : String
}

/// Sendable container for RenderInput and ImageBuffer
/// Used to share one picture to threads without mutexes or Rc.
/// 'Cause mutexes or Rc will break parallelism
struct ThreadData {
    conf_link : *const RenderInput,
    pic_link  : *mut ImageBuffer<Rgb<u8>, Vec<u8>>
}

unsafe impl Sync for ThreadData {}
unsafe impl Send for ThreadData {}

/// Render for one thread
/// `count` - total thread count
/// `id` - this thread id
fn render_fun(data : ThreadData , count : u32, id : u32) {
    let input : &RenderInput = unsafe{ &*data.conf_link };
    let width   = input.width;
    let height  = input.height;
    let x_dist  = input.x_dist;
    let x_from  = input.x_from;
    let y_from  = input.y_from;
    let y_dist  = input.y_dist;
    let depth   = input.depth;
    let fdepth  = depth as f64;
    let x0      = input.x0;
    let y0      = input.y0;
    // let conf    = &input.conf;
    let fun : &RenderFun = match &*input.fun {
        "jul" => &julia,
        "man" => &mandelbort,
        _     => {
            println!("fun: use jul or man");
            return
        }
    };
    let img     = unsafe{ &mut *data.pic_link };
    let mut buffers = Buffers::new(input.colors.len());
    for x in 0 .. width {
        let fx = (x as CrdFloat / width as CrdFloat) * x_dist + x_from;
        if x % count == id {
            for y in 0 .. height {
                let fy = (y as CrdFloat / height as CrdFloat) * y_dist + y_from;
                let val = fun(x0, y0, fx, fy, depth) as f64;
                let intense = val / fdepth;
                let intense = lagrange(&input.polynom, intense);
                let clr = bezier(&mut buffers, &input.colors, intense);
                img.put_pixel(x, y, Rgb(clr));
            }
        }
    }
}

fn main() {
    let args : Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("args: params_file");
        return
    }
    let params = match Params::load_file(&args[1]) {
        Ok(a) => a,
        Err(e) => {
            println!("read params error: {:?}", e);
            return
        }
    };
    let mut img = ImageBuffer::new(params.size, params.size);
    let render_input = RenderInput {
        x_from  : params.x_from,
        x_dist  : params.x_dist,
        y_from  : params.y_from,
        y_dist  : params.y_dist,
        x0      : params.x0,
        y0      : params.y0,
        width   : params.size,
        height  : params.size,
        colors  : params.colors.clone(),
        polynom : params.intense_polynom.clone(),
        fun     : params.fun.clone(),
        depth   : params.depth
    };
    let mut handlers = vec![];
    let thread_count = params.thread_count;
    for i in 0 .. thread_count {
        let data = ThreadData {
            conf_link : &render_input,
            pic_link : &mut img
        };
        let handler = thread::spawn(move||{
            render_fun(data, thread_count, i);
        });
        handlers.push(handler)
    }
    for handler in handlers {
        if let Err(e) = handler.join() {
            panic!("{:?}", e)
        }
    }

    let status = img.save(params.outfile);
    println!("{:?}", status);
}

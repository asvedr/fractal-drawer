use std::mem;

/// Two swapable buffers for `bezier`
pub struct Buffers {
    a : Vec<[u8; 3]>,
    b : Vec<[u8; 3]>
}

#[derive(Clone)]
pub struct Point {
    pub x : f64,
    pub y : f64
}

impl Buffers {
    pub fn new(size : usize) -> Buffers {
        let mut bufs = Buffers {
            a : vec![],
            b : vec![]
        };
        for _ in 0 .. size {
            bufs.a.push([0, 0, 0]);
            bufs.b.push([0, 0, 0]);
        }
        bufs
    }
}

/// bezier point in color space
/// Buffers used as parameter to less alloc/decalloc
pub fn bezier(buffers : &mut Buffers, colors : &Vec<[u8; 3]>, intense : f64) -> [u8; 3] {
    let mut folded = &mut buffers.a;
    let mut source = &mut buffers.b;
    for i in 0 .. colors.len() {
        source[i] = colors[i];
    }
    let mut current_len = colors.len();
    // Folding step by step
    while current_len > 1 {
        for i in 0 .. current_len - 1 {
            let a = source[i];
            let b = source[i+1];
            let c = &mut folded[i];
            // Calculating new point
            for i in 0 .. 3 {
                let component = ((b[i] as f64 - a[i] as f64) * intense) + a[i] as f64;
                (*c)[i] = component as u8;
            }
        }
        current_len -= 1;
        mem::swap(&mut source, &mut folded);
    }
    source[0]
}

/// points - data for polynom. X - argument. Y - result
pub fn lagrange(pts : &[Point], x : f64) -> f64 {
    let mut sum = 0.0;
    let len = pts.len();
    for i in 0 .. len {
        let mut mul = 1.0;
        for j in 0 .. len {
            if j == i {
                continue
            }
            mul *= (x - pts[j].x) / (pts[i].x - pts[j].x);
        }
        sum += pts[i].y * mul;
    }
    sum
}

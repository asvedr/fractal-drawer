# Drawer for Mandelbort and Julia fractals

![Sample Picture](sample1.png?raw=true "Sample")

# Generator
picgen dir. Written in rust. Controlled by config file. Rendering quad fractal picture in multithread and save it to png.
```
cd picgen
cargo build --release
cp target/release/picgen ../ui
```
requirements:
  * image

# UI
ui dir - only UI written in python-tkinter. python2 python3 compatible. Can be compiled with cx_Freeze. Generator executable must be in folder with main.py  
Before run put the `picgen` near main.py.
To run:
```
python ui/main.py
```
To build .exe:
```
cd ui
python setup.py build
cp picgen build/<your-exe>/
```
requirements:
  * tkinter
  * PIL(Pillow for windows)
  * cx_Freeze(for building executable)
